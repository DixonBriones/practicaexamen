package facci.dixonbriones.practica2005;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditarActivity extends AppCompatActivity {
    EditText EditTextmarca,EditTextmodelo,EditTextprecio,EditTextfoto;
    Button actualizarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        String idCelular ="";
        Bundle extras = getIntent().getExtras();
        if(extras!= null){
            idCelular= extras.getString("IdCelular");
        }


        EditTextmarca = findViewById(R.id.EditTextmarcaEditar);
        EditTextmodelo=findViewById(R.id.EditTextmodeloEditar);
        EditTextprecio=findViewById(R.id.EditTextprecioEditar);
        EditTextfoto=findViewById(R.id.EditTextfotoEditar);


        actualizarButton=findViewById(R.id.actualizarButton);

        Celular celular = Celular.findById(Celular.class, Long.parseLong(idCelular));

        EditTextmarca.setText(celular.marca);
        EditTextmodelo.setText(celular.modelo);
        EditTextprecio.setText(celular.precio);
        EditTextfoto.setText(celular.foto);

        actualizarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                celular.marca = EditTextmarca.getText().toString();
                celular.modelo = EditTextmodelo.getText().toString();
                celular.precio = EditTextprecio.getText().toString();
                celular.foto = EditTextfoto.getText().toString();
                celular.save();
                Intent intent= new Intent(EditarActivity.this , RecyclerActivity.class);
                startActivity(intent);
            }
        });
    }
}