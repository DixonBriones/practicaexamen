package facci.dixonbriones.practica2005;

import android.text.Editable;

import com.orm.SugarRecord;

public class Celular extends SugarRecord<Celular> {
    String marca;
    String modelo;
    String precio;
    String foto;

    public Celular() {
    }

    public Celular(String marca, String modelo, String precio, String foto ){
        this.marca = marca;
        this.modelo = modelo;
        this.precio= precio;
        this.foto= foto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }


}
