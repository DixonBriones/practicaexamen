package facci.dixonbriones.practica2005;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import facci.dixonbriones.practica2005.adapters.CelularAdapter;

public class RecyclerActivity extends AppCompatActivity {
    RecyclerView recyclerViewCelular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        recyclerViewCelular=findViewById(R.id.recyclerViewCelular);

        //Coleccion de datos
        List<Celular> listarcelular = Celular.listAll(Celular.class);
        listarcelular.forEach((item)-> Log.e("Modelo",item.getModelo()));
        //Layout

        recyclerViewCelular.setLayoutManager(new LinearLayoutManager(this));

        //Adaptador
        CelularAdapter celularAdapter= new CelularAdapter(listarcelular,this);

        recyclerViewCelular.setAdapter(celularAdapter);



    }
}