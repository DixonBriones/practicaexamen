package facci.dixonbriones.practica2005.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import facci.dixonbriones.practica2005.Celular;
import facci.dixonbriones.practica2005.EditarActivity;
import facci.dixonbriones.practica2005.MainActivity;
import facci.dixonbriones.practica2005.R;
import facci.dixonbriones.practica2005.RecyclerActivity;

public class CelularAdapter extends RecyclerView.Adapter<CelularAdapter.CelularViewHolder>{

    List<Celular> listarcelular;
    Context context;


    public CelularAdapter(List<Celular> listarcelular, Context context) {
        this.context = context;
        this.listarcelular = listarcelular;
    }

    @NonNull
    @Override
    public CelularViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlayout, parent, false);
        return new CelularViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CelularViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.textViewPrecio.setText(listarcelular.get(position).getPrecio());
        holder.textViewModelo.setText(listarcelular.get(position).getMarca()+" "+listarcelular.get(position).getModelo());
        Picasso.get().load(listarcelular.get(position).getFoto()).into(holder.imageCelular);

        holder.buttonElimina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listarcelular.get(position).delete();
                Intent intent= new Intent(context, RecyclerActivity.class);
                context.startActivity(intent);
            }
        });
        holder.buttonEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(context, EditarActivity.class);
                intent.putExtra("IdCelular", listarcelular.get(position).getId().toString());
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return listarcelular.size();
    }


    public class CelularViewHolder extends RecyclerView.ViewHolder{

        ImageView imageCelular;
        TextView textViewModelo,textViewPrecio;
        Button buttonElimina,buttonEditar;

        public CelularViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewModelo=itemView.findViewById(R.id.textViewModelo);
            textViewPrecio=itemView.findViewById(R.id.textViewPrecio);
            imageCelular= itemView.findViewById(R.id.imgCelular);

            buttonElimina= itemView.findViewById(R.id.buttonEliminar);
            buttonEditar = itemView.findViewById(R.id.buttonEditar);

        }
    }
}
