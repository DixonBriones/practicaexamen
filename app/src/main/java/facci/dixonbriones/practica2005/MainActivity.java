package facci.dixonbriones.practica2005;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText EditTextmarca,EditTextmodelo,EditTextprecio,EditTextfoto;
    Button guardarbutton,listarbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditTextmarca = findViewById(R.id.EditTextmarca);
        EditTextmodelo=findViewById(R.id.EditTextmodelo);
        EditTextprecio=findViewById(R.id.EditTextprecio);
        EditTextfoto=findViewById(R.id.EditTextfoto);

        guardarbutton=findViewById(R.id.guardarbutton);
        listarbutton=findViewById(R.id.listarbutton);

        guardarbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Celular celular= new Celular(
                        EditTextmarca.getText().toString(),
                        EditTextmodelo.getText().toString(),
                        EditTextprecio.getText().toString(),
                        EditTextfoto.getText().toString()
                );

                celular.save();

                Log.e("datos","Datos guardados" );
            }
        });

        listarbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this,RecyclerActivity.class);
                startActivity(intent);
            }
        });


    }
}